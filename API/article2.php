<?php
/**examples of using namespace & use in php */

/**include all class */
require_once('../core/Database.php');
require_once('../core/Response.php');

use Core\Database;
use Core\Response;

class Article
{
    public $mysql;
    public $response;
    
    public function __construct()
    {
        $database = new Database();
        $this->mysql = $database->connect();

        $this->response = new Response();
        $this->determineMethod();
    }

    public function determineMethod()
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                # code...
                $data = [];
                $articles = $this->mysql->query('SELECT * from `articles`');
                if($articles->num_rows > 0){
                    foreach ($articles as $key => $article) {
                        # code...
                        $data[] = $article; 
                    }
                }
        
                $this->response->returnJSON($data);
                break;
            
            case 'POST':
                # code...
                $title = $_POST['title'] ?? '';
                $body  = $_POST['body'] ?? '';
                $author_id  = $_POST['author_id'] ?? 1;
        
                $sql = "INSERT INTO `articles` (`title`, `body`, `author_id`) VALUES ('$title', '$body', '$author_id')";
                
                if($this->mysql->query($sql)){
                    $this->response->returnJSON('Data Added successfully', 201);
                }else{
                    $this->response->returnJSON('internal Server Error', 500);
                }
                break;
        
            case 'PUT':
            case 'PATCH':
            case 'DELETE':
                break;
        
            default:
                echo json_encode(['status' => 'ok']);
                break;
        }
    }
}

$article = new Article();