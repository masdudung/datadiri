<?php
header('Content-Type: application/json; charset=utf-8');

$novan = new stdClass();
$novan->nama = 'novan';
$novan->saldo = 10000;

$jani = new stdClass();
$jani->nama = 'jani';
$jani->saldo = 20000;

$nando = new stdClass();
$nando->nama = 'nando';
$nando->saldo = 30000;

$siswa = [
    $novan,
    $jani,
    $nando
];

$json = json_encode($siswa);
echo $json;