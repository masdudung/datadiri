<?php
/**include all class */
require_once('../core/Database.php');
require_once('../core/Response.php');

/**init class & connect to database */
$response = new Response();
$database = new Database();
$mysql = $database->connect();

/**determine request method */
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        # code...
        $data = [];
        $articles = $mysql->query('SELECT * from `articles`');
        if($articles->num_rows > 0){
            foreach ($articles as $key => $article) {
                # code...
                $data[] = $article; 
            }
        }

        $response->returnJSON($data);
        break;
    
    case 'POST':
        # code...
        $title = $_POST['title'] ?? '';
        $body  = $_POST['body'] ?? '';
        $author_id  = $_POST['author_id'] ?? 1;

        $sql = "INSERT INTO `articles` (`title`, `body`, `author_id`) VALUES ('$title', '$body', '$author_id')";
        
        if($mysql->query($sql)){
            $response->returnJSON('Data Added successfully', 201);
        }else{
            $response->returnJSON('internal Server Error', 500);
        }
        break;

    case 'PUT':
    case 'PATCH':
    case 'DELETE':
        break;

    default:
        echo json_encode(['status' => 'ok']);
        break;
}