<?php

namespace Core;

class Response
{
    public $response = [
        'status' => 'ok'
    ];

    function __construct()
    {
        /**setting response header as json */
        header('Content-Type: application/json; charset=utf-8');
    }

    public function returnJSON($data, $httpCode = 200)
    {
        $this->setHttpCode($httpCode);
        $response['message'] = $data;
        echo json_encode($response);
    }

    public function setHttpCode($code)
    {
        http_response_code($code);
    }
}
