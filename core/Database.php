<?php

namespace Core;

class Database
{

    private $hostname = 'localhost';
    private $username = 'root';
    private $password = '';
    private $db = 'latihansalt';
    public $conn;

    public function connect(){
        $this->conn = mysqli_connect($this->hostname, $this->username, $this->password, $this->db);
        if (!$this->conn) {
            echo "koneksi gagal";
        }

        return $this->conn;
    }
}