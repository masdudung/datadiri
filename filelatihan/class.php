<?php

class Mobil {
    public $color;
    public $plat;

    public function setPlat($_plat){
        $this->plat = $_plat;
    }

    public function setColor($_color="hitam"){
        $this->color = $_color;
        $this->setPlat('n 0001 ab');
    }

    public function getColor(){
        return $this->color;
    }

    public function getPlat(){
        return $this->plat;
    }
}

// $avanza = new Mobil();
// $avanza->setColor('merah');
// $avanza->setPlat('lalala');
// $warnaAvanza = $avanza->getColor();
// $platAvanza = $avanza->getPlat();
// echo "avanza : " . $warnaAvanza . " " . $platAvanza . "<br>";

// $pajero = new Mobil();
// $pajero->setColor('hijau');
// echo "pajero : " . $pajero->color . "<br>"; 


// $crv = new Mobil();
// $crv->setColor();
// echo "crv : " . $crv->color . "<br>"; 


// $hernando_apriyanus;
// $hernandoApriyanus;