<!DOCTYPE html>
<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="css/index.css">
    <style>
        span.error {
            color: red;
        }
    </style>
</head>


<body>
    <!-- konten -->
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-2">
                <br>
                <h2>Login Page</h2>
                <hr>
                <br><br>
                <form action="processlogin.php" method="POST">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input name="password" type="text" class="form-control" id="exampleInputPassword1">
                    </div>
                    <?php
                    $login = $_GET['login'] ?? null;
                    if ($login == 'error') {
                        echo "<span class='error'>username atau password salah</span>";
                    }else if ($login == 'not-login'){
                        echo "<span class='error'>Anda belum login</span>";
                    }
                    ?>
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <!-- footer -->
</body>

</html>